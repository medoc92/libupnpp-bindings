#!/bin/sh
# Adding this to meson would add a lot of complexity for little gain as we have the output in the
# version control and tar files. Just run the script when working on the .i files (if ever).

swig -c++ -python -I/usr/include -o upnpp_wrap.cxx upnpp-python.i
