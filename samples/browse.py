#!/usr/bin/python3

import sys
import os
import upnpp

from samplecommon import debug as debug
from samplecommon import info as msg

# This implement recursive browsing. Limit depth to avoid being too hard on the server
maxdepth = 3

def usage():
    prog = os.path.basename(__file__)
    msg(f"Usage: {prog} <devname>")
    sys.exit(1)

def recursivebrowse(srv, depth, id):
    depth = depth + 1
    if depth > maxdepth:
        return
    indent = (depth-1) * "    "
    data = upnpp.runaction(srv, "Browse", [id, "children", "", "0", "10", ""])
    entries = data["Result"]
    dirc = upnpp.UPnPDirContent()
    dirc.parse(entries)

    for dirobj in dirc.m_items:
        print(indent + f"{dirobj.m_title}")

    for dirobj in dirc.m_containers:
        print(indent + f"{dirobj.m_title}/ \t\tobjid {dirobj.m_id}")
        if not dirobj.m_title.startswith(">>"):
            recursivebrowse(srv, depth, dirobj.m_id)

    depth = depth-1
    
if len(sys.argv) != 2:
    usage()
devname = sys.argv[1]

log = upnpp.Logger_getTheLog("stderr")
log.setLogLevel(2)

srv = upnpp.findTypedService(devname, "contentdirectory", True)
if not srv:
    msg(f"device {devname} not found or contentdirectory not available\n")
    sys.exit(1)

recursivebrowse(srv, 0, "0")

