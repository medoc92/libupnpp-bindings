#!/usr/bin/python3

import sys
import upnpp

def debug(x):
   print("%s" % x, file = sys.stderr)
def usage():
   debug("Usage: getmedinfo.py devname")
   sys.exit(1)
   
if len(sys.argv) != 2:
   usage()
devname = sys.argv[1]

srv = upnpp.findTypedService(devname, "connectionmanager", True)

if not srv:
   debug("findTypedService failed")
   sys.exit(1)

retdata = upnpp.runaction(srv, "GetProtocolInfo", [])

sinkinfo = retdata["Sink"]
print("Sink Info: %s" % sinkinfo)

