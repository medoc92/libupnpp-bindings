#!/usr/bin/python3

import sys
import time
import upnpp

def debug(x):
   print("%s" % x, file = sys.stderr)
def usage():
   debug("Usage: seektest.py devname")
   sys.exit(1)
   
if len(sys.argv) != 2:
   usage()
devname = sys.argv[1]

srv = upnpp.findTypedService(devname, "avtransport", True)

if not srv:
   debug("findTypedService failed")
   sys.exit(1)

uri = "http://192.168.1.101:9090/Music/mp3/variete/acdc/back_in_black/01%20-%20Hells%20Bells.mp3"
upnpp.runaction(srv, "SetAVTransportURI", ["0", uri, ""])
# Note: mpd does not support seek on stopped track
upnpp.runaction(srv, "Play", ["0", "1"])
#upnpp.runaction(srv, "Seek", ["0", "REL_TIME", "0:0:40"])
#upnpp.runaction(srv, "Stop", ["0",])

time.sleep(2)
retdata = upnpp.runaction(srv, "GetPositionInfo", ["0"])
print(f"RelTime {retdata["RelTime"]}")
