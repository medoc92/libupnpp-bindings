#!/usr/bin/python3
# Connect to a device avtransport service and decode transportstate events
import sys
import time
import upnpp
from xml.sax import parseString
from xml.sax.handler import ContentHandler

def msg(x):
   print("%s" % x, file = sys.stderr)
def usage():
   msg("Usage: events-tpstate.py <device> \n")
   sys.exit(1)
   

class XmlHandler(ContentHandler):
   def startElement(self, name, attrs):
      #msg(f"startElement [{name}]")
      if name.lower() == "transportstate":
         try:
            global transportstate
            transportstate = attrs.getValue("val")
            msg(f"transportstate is now {transportstate}")
         except Exception as ex:
            msg("NO val attr in TransportState element: {ex}")
      #for nm,val in attrs.items():
      #   msg(f"{nm} -> {val}")

class EventReporter:
   def upnp_event(self, nm, value):
      #msg(f"Event: {nm}")
      if nm == "LastChange":
         parseString(value, handler)

if len(sys.argv) != 2:
   usage()
devname = sys.argv[1]
fuzzyservicename = "avtransport"

srv = upnpp.findTypedService(devname, fuzzyservicename, True)
if not srv:
    msg("findTypedService failed")
    sys.exit(1)


reporter = EventReporter()
handler = XmlHandler()

# You do need to store the result of installReporter
bridge = upnpp.installReporter(srv, reporter)

while True:
   time.sleep(20000)
