#!/usr/bin/python3

import sys
import upnpp

def deb(x):
   print("%s" % x, file = sys.stderr)

if len(sys.argv) != 2:
   deb("Usage ohListSources.py <devname>")
   sys.exit(1)
devname = sys.argv[1]

log = upnpp.Logger_getTheLog("stderr")
log.setLogLevel(2)

srv = upnpp.findTypedService(devname, "product", True)
if not srv:
   deb(f"'Product' service not found: device {devname} is not openhome or not found")
   sys.exit(1)

retdata = upnpp.runaction(srv, "SourceXml", [])

print(f"{retdata}")

sys.exit(0)
